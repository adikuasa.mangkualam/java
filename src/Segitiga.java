public class Segitiga {

    public int panjang = 10;
    public int tinggi = 5;
    public int lebar = 5;

    public Segitiga(int panjang,int tinggi,int lebar){
            this.panjang = panjang;
            this.tinggi = tinggi;
            this.lebar = lebar;
    }//constructor

    public int volume(int p,int t,int l){
        return p*t*l;
    }//method

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public int getTinggi() {
        return tinggi;
    }

    public void setTinggi(int tinggi) {
        this.tinggi = tinggi;
    }

    public int getLebar() {
        return lebar;
    }

    public void setLebar(int lebar) {
        this.lebar = lebar;
    }
}
