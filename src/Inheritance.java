public class Inheritance {

    static class KelasA{
        public String atributA;

        public int getAtributA() {
            return 0;
        }

        public void setAtributA(String atributA) {
            this.atributA = atributA;
        }
    }

    static class KelasB extends KelasA{
        private String atributB;

        public int getAtributB() {
            return 0;
        }

        public void setAtributB(String atributB) {
            this.atributA = atributB;
        }
    }
    public static void main(String[] args) {
        KelasB kelasB = new KelasB();
        kelasB.setAtributB("makan");
        kelasB.getAtributB();

    }
}
