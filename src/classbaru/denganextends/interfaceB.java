package classbaru.denganextends;

public interface interfaceB {

    public void methodA(); // interface method (does not have a body)
    public void methodB(); // interface method (does not have a body)
}
